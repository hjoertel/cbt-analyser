#!/usr/bin/tclsh8.6
#
# CANbus Triple
#

package require Tk

# Configure your serial port here
#
set Term(Port) /dev/ttyACM0
set Term(Mode) "57600,n,8,1"
set usbchannel {}	;# storage for USB file descriptor, used globally

# Configure the CANBus-Triple application
#
set CBT(name)	  "CANBus-Triple Analyser"
set CBT(Version)  "0.9"
set CBT(configfile) ".cbt.rc"
set CBT(viewMode) trace  ;# Standard mode is trace, can be switched to Object-View mode
set CBT(Font)	  Courier

# ##########################################################################
# end of local configuratio
#
# look if the configuration should be overwritten by the content of the 
# configuration file
#
proc loadConfig file {
}

puts "File $::CBT(configfile): $CBT(configfile)"
if { [ file exists $::CBT(configfile)] } {
    puts "load ........"
    loadConfig $::CBT(configfile)
}

# load utilities
source library/tools.tcl
source library/fileio.tcl

# Global variables
#
set CBT(LogArea) {}


# bit rate fixed so far to 57600 8N1
proc initDevice {name} {
}


proc getInfo {} {
global usbchannel
    puts -nonewline $usbchannel \x01\x01
    # flush is not needed if outpu buffering is none
    flush $usbchannel
}
proc getEepromValue {} {
global usbchannel
    puts -nonewline $usbchannel \x01\x02
}
proc restoreEepromStockValue {} {
global usbchannel
    puts -nonewline $usbchannel \x01\x04
}
proc printBusDebug {bus} {
global usbchannel
    #puts -nonewline $usbchannel \x01\x10\x0$bus
    puts -nonewline $usbchannel \x01\x10\x01
    puts -nonewline $usbchannel \x01\x10\x02
    puts -nonewline $usbchannel \x01\x10\x03
}


# Enable logging on Bus 1 and disable filtering
# cmd	bus	on/off  id	mask
# 0x03 	0x01 	0x01 	0x0000 	0x0000 
proc  enableLog {bus} {
global usbchannel
    #puts -nonewline $usbchannel \x03\x01\x02\x00\x00\x00\x00\x00\x00\x00\x00
    puts -nonewline $usbchannel "\x03\x01\x02\x00\x00\x00\x00"
    puts -nonewline $usbchannel "\x03\x02\x02\x00\x00\x00\x00"
    puts -nonewline $usbchannel "\x03\x03\x02\x00\x00\x00\x00"
    flush $usbchannel
}

# Command 	Bus Id 	Message ID 	Data				Length
# 0x02		01	290 		00 00 00 00 00 00 00 00 	8
proc sendMsg {bus} {
global usbchannel
    puts -nonewline $usbchannel \x02\x01\x05\x55\x11\x22\x33\x44\x55\x66\x77\x88\x08
}

proc reboot {} {
global usbchannel
    puts -nonewline $usbchannel \x01\x16
}


##################### Terminal In/Out events ############################
proc term_out { usbchannel key } {
    switch -regexp -- $key {
        [\x07-\x08] -
        \x0D        -
        [\x20-\x7E] { puts -nonewline $usbchannel $key; return -code break }
        [\x01-\x06] -
        [\x09-\x0C] -
        [\x0E-\x1F] -
        \x7F        { return }
        default     { return }
    } ;# switch
}

proc term_in { ch } {
    upvar #0 CBT(LogArea) log

    #switch -regexp -- $ch {
    #    \x07    { bell }
#	{0a}    { # ignore }
#	{0d}    { $log insert end "\n" }
#        default { $log insert end $ch }
#    }
    $log insert end $ch
    $log see end
}

set rxState 0	;# idle
set rxStep  0	;# idle
set canId   0	;# CAN frame Id
set length  0	;# frame length in data byte
set dBytes {}
set timeStamp 0
set channel 0	;# CAN channel the frame was received on


proc procRxChar {ch} {
    global rxState
    global rxStep
    global canId
    global dBytes
    global length
    global timestamp
    global channel


    switch $rxState {
	0 {
	    # puts "0x03 seen"
	    if { $ch == "\x03" } {
		set rxState 1
		set rxStep 0
		#puts "start"
		set timestamp [clock milliseconds]
	    }
	    if { $ch == "\x7b" } { ;# looking for opening brace
		puts "got status"
		term_in "\{"
		set rxState 2
		set rxStep 0
	    } 
	}
	1 {
	    # 01::02::20::02::0a::ca::00::00::02::89::00::01::01::0a
	    # channel {hid lid} {8x byte} len
	    switch $rxStep {
		0 {
		    binary scan $ch c channel
		    #puts "received frame from channel $channel" 
		}
		1 { ;# high id
		    binary scan $ch c canId
		    # binary scanwith format c returns a signed, next line
		    # converts to unsigned
		    set canId [expr { $canId & 0xff }]
		    #puts ">$canId<"
		}
		2 { ;#low canId
		    binary scan $ch c canIdL
		    set canIdL [expr { $canIdL & 0xff }]
		    set canId [expr ($canId << 8) + $canIdL]
		    #puts ">>$canId><"
		}
		3 { ;# byte 0
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		4 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		5 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		6 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		7 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		8 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		9 {
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		10 { ;# byte 8
		    binary scan $ch H2 byte
		    append dBytes "$byte "
		}
		11 { ;# len
		    binary scan $ch c length
		    #puts "received frame length $length" 
		}
		12 { ;# no idea what this byte is used for
		}
		default {
		    # assemble the CAN frame information
		    # puts "Am Ende [binary encode hex $ch]   "
		    # puts "\n$canId:$ $dBytes" 
		    set ts [format "%s.%03d" \
			[clock format [expr {$timestamp / 1000}] -format %T] \
			[expr {$timestamp % 1000}] \
		    ]
		    set out "$ts \[$channel\] "
		    append out "[format %5d $canId]/0x[format %04x $canId]: [format %2d $length]: "
		    if { $length  > 8 } { set length 9}  
		    for {set i 0} { $i < $length } {incr i} {
			append out "[lindex $dBytes $i] "
		    }
		    # puts $out
		    term_in "$out\n"

		    set rxState 0
		    set rxStep 0
		    set dBytes {}
		}
	    }
	    incr rxStep
	}
	2 {
	    term_in "$ch"
	    if { $ch == "\x7d" } {
		term_in "\n"
		set rxState 0
	    }	

	}

    }
}


# receiver called by fileevent whenever the input is readable 
# usbchannel is the input channel to read from
# all lines coming from cbt are line ended 0d 0a
# BUT inbetween a message there can be data bytes with value od or oa as well
# so we read until od oa but have to check if all data are there based on the
# first byte of amessage indicating the received message
# e.g. 0x03 expects all 16 bytes
proc receiver {usbchannel} {
    
    set bytes [split [read $usbchannel] {} ]
    set length [llength $bytes]

    for { set i 0} { $i < $length } { incr i} {
	# puts -nonewline ":[binary encode hex [lindex $bytes $i]]:"
	procRxChar [lindex $bytes $i]
    }
    #puts ">>$length"
}


##################### Windows ############################
proc scrolled_text { f args } {
    frame $f
    eval {text $f.text \
        -xscrollcommand [list $f.xscroll set] \
        -yscrollcommand [list $f.yscroll set]} $args
    scrollbar $f.xscroll -orient horizontal \
        -command [list $f.text xview]
    scrollbar $f.yscroll -orient vertical \
        -command [list $f.text yview]
    grid $f.text $f.yscroll -sticky news
    grid $f.xscroll -sticky news
    grid rowconfigure    $f 0 -weight 1
    grid columnconfigure $f 0 -weight 1
    return $f.text
}

##### main #######
proc main { } {
global argv0 argv argc env
global usbchannel
variable CBT
#variable widgets

    #wm withdraw .

# prepare some option that might be used with the application
    set options {
	{-autoconnect.arg.secret  "1" "connect directly after at startup"}
	{-script.arg.secret       ""  "load script directly at startup"}
	{-hide.arg.secret         "0" "hide main window"}
	{-config-path.arg         ""  "path to di/loadrectory with configuration files"}
#	{-debug.arg         	  "0"  "activates debug mode"}
    }
    
# open the serial Interface 
set usbchannel [open $::Term(Port) r+]
#fconfigure $usbchannel -mode $Term(Mode) -translation binary -buffering none -blocking 0
# will wait for a comple buffer, which for read CAN messages is 16
fconfigure $usbchannel -mode $::Term(Mode) -translation {crlf binary} \
		-buffersize 100 -buffering none -blocking 0
# set up the fileevent procedure
fileevent $usbchannel readable [list receiver $usbchannel]
puts "serial port $::Term(Port) opened as \$usbchannel = $usbchannel"

set ::CBT(LogArea) [scrolled_text .t -width 80 -height 25 -font $CBT(Font) ]
pack .t -side top -fill both -expand true

bind $::CBT(LogArea) <Any-Key> [list term_out $usbchannel %A]

getInfo 
printBusDebug 1
enableLog 1
catch {console hide}

setWindowTitle .
every getInfo 4000

}

main

