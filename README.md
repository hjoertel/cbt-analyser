# CANBus-Triple CAN Analyser

[![N|Solid](https://canb.us/images/logo.svg)](https://canb.us/) CANBus-Triple is a [CAN](https://en.wikipedia.org/wiki/CAN_bus) to USB interface developed as Kickstarterproject in 2014.
It can be connected to three independent CAN interfaces.

**cbt-analyser** connects to CANBus-Trible via USB using a serial protocol. It displays messages received at all of the three CAN buses in a human readable form.
For programming the algorithms and the graphical user interface [Tcl/Tk](https://tcl.tk/) is used.

## Start

If Tcl/tk is installed
```sh
$ ./cbt.tcl
```
May be you have to edit the first line of _cbt.tcl_ to match the Tcl interpreter on your system
```sh
#!/usr/bin/tclsh8.6
```


## ToDoos
 - store the configuration in an file called .cbt.rc in the current directory orvt the users home directory 
 - graphical configuration
 - logging data in a format where it can be anlyesd with tools like the emtas GmbH [CANinterpreter](http://www.emtas.de/en/produkte/can-interpreter) or [SavvyCAN](http://www.savvycan.com/).
 - enable tranmist CAN messages using the GUI  