# 
# every --
# 
# Runs a script every $ms milliseconds
#
proc every { script { ms 1000 } } {
    eval $script
    after $ms [info level 0]
}

#
# setWinTitle --
#
proc setWindowTitle { w } {
global CBT
    wm title . "$::CBT(name) V$::CBT(Version)"
}

