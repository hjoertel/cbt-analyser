#
# configuration tools
# mostly file related, storing and reloading data, export csv values etc

#######################################################################
# save actual settings 
# called from the file menu
proc saveConfig {filename} {
global CBT Term tcl_platform widgets symbols

    if { [catch {open $filename w} config_fd] == 1 } {
        set msg "Can not open configuration file $filename"
        tk_messageBox -title "Save Config" -message $msg -type ok 
    } else {

	puts $config_fd "# $filename\n"
	puts $config_fd "# CANBus-Triple analyser configuration file"
	puts $config_fd "#\n# these are simple Tcl set variable commands overwriting the variables"
	puts $config_fd "# in the main program"
	puts $config_fd "set Term(Port) $Term(Port)"
	puts $config_fd "#"
	puts $config_fd "set CBT(viewMode) $ST(viewMode)"

	close $config_fd
    }
}


#
# load configuration from file
#
## simply source the file for now.
#
#  A better approach is using a Save Interpreter to load
#  and to evaluate the  code in the configuration script
proc loadConfig {filename} {
    global CBT Term widgets

if 1 {
    source $filename

} else {
    if {[file exists $filename]} {
	set config_fd [open $filename r]
    } else {
	return
    }
    # read the complete file into a variable
    set configScript [read $config_fd]
    close $config_fd

    #  Create a safe interp to evaluate the
    #  code in the configuration script
    set si [interp create -safe]
}
}
